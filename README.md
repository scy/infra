# infra - my personal infrastructure

This repository contains configuration files and scripts I use for my personal digital infrastructure.

It's not terribly well documented right now, this might change gradually.
Feel free to ask questions, either as an [issue](https://codeberg.org/scy/infra/issues) or by contacting me in any other way, e.g. via [Mastodon](https://chaos.social/@scy).

## Components

The following areas exist in this repo:

* [`esphome`](esphome): Smart home device configuration files based on [ESPHome](https://esphome.io/).
* [`hosts`](hosts): Bootstrapping instructions or details about some of my computers.
* [`salt`](salt): "Infrastructure as code" files based on [Salt](https://saltproject.io/) that set up my machine(s).
