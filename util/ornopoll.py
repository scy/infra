from dataclasses import dataclass

import minimalmodbus
import serial


@dataclass
class Reading:
    frequency: float  # Hz
    voltage: float    # V
    current: float    # A
    act_power: int    # W
    react_power: int  # var
    app_power: int    # VA
    power_fac: float


def read_values(orno):
    return Reading(
        frequency=orno.read_register(0x130, 2),
        voltage=orno.read_register(0x131, 2),
        current=orno.read_long(0x139) / 1000,
        act_power=orno.read_long(0x140),
        react_power=orno.read_long(0x148),
        app_power=orno.read_long(0x150),
        power_fac=orno.read_register(0x158, 3),
    )


# According to registers list: RTU 8N1, 9600 baud.
# That's lying though, in reality the device is using even parity.
orno = minimalmodbus.Instrument("/dev/ttyS13", 1)
orno.serial.baudrate = 9600
orno.serial.bytesize = 8
orno.serial.parity = serial.PARITY_EVEN
orno.serial.stopbits = serial.STOPBITS_ONE
orno.serial.timeout = 0.2

while True:
    print(read_values(orno))
