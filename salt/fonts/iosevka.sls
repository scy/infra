install Iosevka:
  archive.extracted:
    - name: /usr/share/fonts/truetype/iosevka
    - if_missing: /usr/share/fonts/truetype/iosevka/iosevka.ttc
    - source: https://github.com/be5invis/Iosevka/releases/download/v22.0.1/super-ttc-iosevka-22.0.1.zip
    - source_hash: 7ac7cdf899ff829637d532733f7ec16111f849d1df538262d8a6295c8ae1d6aac9ca745f1f8676d8d76d87cbbe0c8737c0a3ed0beaf0c0fb0ae4f042d063f0c7
    - enforce_toplevel: false
  cmd.run:
    - onchanges:
        - archive: install Iosevka
    - name: fc-cache -f

set GNOME to use Iosevka:
  file.managed:
    - require:
        - file: dconf user profile
        - archive: install Iosevka
    - name: /etc/dconf/db/local.d/50_scy_iosevka
    - mode: 0644
    - makedirs: true
    - contents: |
        [org/gnome/desktop/interface]
        monospace-font-name='Iosevka Fixed Regular 11'
  cmd.run:
    - onchanges:
        - file: set GNOME to use Iosevka
    - name: dconf update
