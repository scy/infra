enable unattended-upgrades:
  debconf.set:
    - name: unattended-upgrades
    - data:
        unattended-upgrades/enable_auto_updates:
          type: boolean
          value: true

auto-update packages:
  pkg.installed:
    - require:
        - debconf: enable unattended-upgrades
    - pkgs:
        - bsd-mailx
        - msmtp-mta
        - unattended-upgrades

/etc/msmtprc:
  file.managed:
    - require:
        - pkg: auto-update packages
    - user: root
    - group: root
    - mode: 0644
    - contents: |
        account default
        tls on
        # The HELO parameter. Default is localhost, Fastmail won't accept that.
        domain {{ grains["fqdn"] }}
        host in1-smtp.messagingengine.com
        auth off
        # With msmtp 1.8.7, we could have "scy-msmtp-%H-%U", but Ubuntu 20
        # ships with 1.8.6. xD
        from scy-msmtp-{{ grains["id"] }}@scy.name

/etc/apt/apt.conf.d/99scy-autoupdate:
  file.managed:
    - require:
        - pkg: auto-update packages
    - user: root
    - group: root
    - mode: 0644
    - contents: |
        Unattended-Upgrade::Mail "scy-autoupdate-{{ grains["id"] }}@scy.name";
        Unattended-Upgrade::Sender "{{ grains["id"] }} Auto-Update <scy-autoupdate-{{ grains["id"] }}@scy.name>";
        Unattended-Upgrade::Automatic-Reboot "false";
