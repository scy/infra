# Allows keyfiles stored on /boot to be placed in the initramfs for
# passwordless booting. As outlined in
# /usr/share/doc/cryptsetup-initramfs/README.initramfs.gz.
# This can be useful if /boot is trustworthy and encrypted.

/etc/cryptsetup-initramfs/conf-hook:
  file.keyvalue:
    - key: KEYFILE_PATTERN
    - value: '"/boot/*.key"'
    - separator: '='
    - append_if_not_found: true

/etc/initramfs-tools/initramfs.conf:
  file.keyvalue:
    - key: UMASK
    - value: '0077'
    - separator: '='
    - append_if_not_found: true
