/etc/sudoers.d/sudo-group-nopasswd:
  file.managed:
    - mode: 0600
    - contents: |
        # Managed by sudo-nopasswd.sls.

        # Grant sudo to all users in "sudo" group without requiring a password.
        # Useful for servers where users only have an SSH key for logging in.
        %sudo ALL=(ALL) NOPASSWD: ALL
