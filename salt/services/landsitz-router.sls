{% set ifgroups = { 10: "uplink", 20: "isolated", 30: "isolated_noingress", 40: "allow_wan", 50: "allow_all", 19140: "scynet" } %}
{# For each subnet (and thus VLAN interface), which netdev group does it belong to? #}
{% set subnets = { 8: "isolated_noingress", 9: "allow_wan", 10: "allow_all", 11: "allow_wan", 12: "isolated", 13: "isolated" } %}

{# The following definitions could probably be derived from `subnets`, but my Jinja is not good enough. #}
{# May access other LANs (allow_all). #}
{% set allow_lan = [10] %}
{# May access the internet (allow_all, allow_wan). #}
{% set allow_wan = [9, 10, 11] %}
{# May _not_ access the internet (isolated, isolated_noingress). #}
{% set disallow_wan = [8, 12, 13] %}

{% set lan_iface = "enp3s0" %}
{% set wan_iface = "enx60a4b7c07538" %}

{% set domain = "landsitz.scy.name" %}
{% set dnsmasq_instances = ["unrestricted", "restricted"] %}

{% macro vlan_if(subnet) -%}
  {{ lan_iface }}.{{ subnet }}0
{%- endmacro %}

{% set interfaces_file = "/etc/network/interfaces.d/landsitz-router" %}
{% set restricted_hosts_file = "/var/lib/misc/restricted.hosts" %}
{% set restricted_hosts_script = "/usr/local/sbin/update-restricted-hosts.sh" %}

# Required packages.
router:
  pkg.installed:
    - pkgs:
        - dnsmasq
        - nftables
        - tshark

# Remove resolvconf. We don't need it and it's only causing problems.
remove resolvconf:
  pkg.removed:
    - pkgs:
        - resolvconf

# Disable dhcpcd, we're going fully static here.
dhcpcd:
  service.dead:
    - enable: false

/etc/resolv.conf:
  file.managed:
    - user: root
    - group: root
    - mode: 0644
    - contents: |
        # Generated by Salt.
        search {{ domain }}
        nameserver 127.0.0.1

/etc/iproute2/group:
  file.append:
    - text:
        {% for num, name in ifgroups.items() %}
        - "{{ num }}  {{ name }}"
        {% endfor %}

{{ interfaces_file }}:
  file.managed:
    - user: root
    - group: root
    - mode: 0644
    - contents: |
        # Generated by Salt.

        # Uplink interface.
        auto {{ wan_iface }}
        iface {{ wan_iface }} inet static
          address 192.168.0.254/24
          gateway 192.168.0.1
          pre-up ip link set dev $IFACE group uplink
        iface {{ wan_iface }} inet6 auto

        # LAN interfaces.
        {% for subnet, group in subnets.items() %}
        auto {{ vlan_if(subnet) }}
        iface {{ vlan_if(subnet) }} inet static
          address 10.115.{{ subnet }}.254/24
          pre-up ip link set dev $IFACE group {{ group }}
        {% endfor %}
  cmd.run:
    - name: ifdown -a; ifup -a
    - onchanges:
        - file: {{ interfaces_file }}

# Disable the "normal", non-instanced dnsmasq.
dnsmasq:
  service.dead:
    - enable: false

# A script to make the DHCP names registered on the "restricted" dnsmasq
# instance available for the "unrestricted" one (by creating a hosts file).
{{ restricted_hosts_script }}:
  file.managed:
    - user: root
    - group: root
    - mode: 0755
    - contents: |
        #!/bin/sh
        set -e

        # This script is supposed to be used as a `dhcp-script` for dnsmasq.

        HOSTS_FILE='{{ restricted_hosts_file }}'

        action="$1"; shift
        mac="$1"; shift
        ip="$1"; shift
        host="$1"

        # Make sure the file exists. Seems to be required for sed's `a`.
        if ! [ -e "$HOSTS_FILE" ]; then
          echo '# Created by {{ restricted_hosts_script }}' > "$HOSTS_FILE"
        fi

        if [ "$ip" = "${ip#10.}" ]; then
        	# Doesn't start with `10.`, is probably IPv6. Ignore.
        	exit 0
        fi

        if [ -z "$host" ]; then
        	# Empty host name. Ignore.
        	exit 0
        fi

        names="$host"
        if [ -n "$DNSMASQ_DOMAIN" ]; then
        	# If FQDN can be constructed, do it.
        	names="$names $host.$DNSMASQ_DOMAIN"
        fi

        case "$action" in
        	add|old)
        		# Add the host to the file or update its IP.
        		sed -i -n -e "/ $host /!p" -e "\$a$ip $names  # $(date)" "$HOSTS_FILE"
        		case "$host" in
        			Bad|Ost|West)
        				# A Reolink camera. Set its time (NTP isn't working atm).
        				. /etc/secrets/set_reolink_time.env
        				export REOLINK_PASSWORD
        				/opt/scy/set_reolink_time/set_reolink_time.py "$ip"
        				;;
        		esac
        		;;
        	del)
        		# Remove the host from the file.
        		sed -i -n -e "/ $host /!p" "$HOSTS_FILE"
        		;;
        	*)
        		# Ignore, unknown action.
        		exit 0
        		;;
        esac

        # Ask the unrestricted dnsmasq to re-read its host files.
        # Else, the change we just did might go unnoticed.
        systemctl reload dnsmasq@unrestricted

# Create two dnsmasq instances: One for VLANs that are allowed to access the
# internet, and one for those which aren't.

{% for instance in dnsmasq_instances %}
{% set instance_subnets = allow_wan if instance == "unrestricted" else disallow_wan %}

/etc/default/dnsmasq.{{ instance }}:
  file.managed:
    - require:
        - pkg: router
    - user: root
    - group: root
    - mode: 0644
    - contents: |
        # Generated by Salt (landsitz-router.sls).
        DNSMASQ_OPTS=--conf-file=/etc/dnsmasq.{{ instance }}
        IGNORE_RESOLVCONF=yes

/etc/dnsmasq.{{ instance }}:
  file.managed:
    - require:
        - pkg: router
        - file: {{ restricted_hosts_script }}
    - user: root
    - group: root
    - mode: 0644
    - contents: |
        # Generated by Salt (landsitz-router.sls).

        # Interfaces to listen on.
      {% for subnet in instance_subnets %}
        interface={{ vlan_if(subnet) }}
      {% endfor %}

        # Don't listen on the uplink.
        except-interface={{ wan_iface }}

        # No DHCP on localhost.
        no-dhcp-interface=lo

        # Explicitly bind _only_ to the interfaces we're supposed to.
        bind-interfaces

        # Don't log excessively.
        quiet-dhcp
        quiet-dhcp6
        quiet-ra

        # Don't read /etc/hosts.
        no-hosts
        # If A records (real or virtual ones) match the interface the request
        # was received on, return only the matching ones.
        localise-queries

        # Make "simple names" (without period) available under a FQDN.
        expand-hosts
        domain={{ domain }}

        # Don't forward guaranteed-local things.
        domain-needed
        bogus-priv
        local=/fritz.box/

        # Improve security.
        # TODO: DNSSEC configuration.
        stop-dns-rebind
        # See CERT VU#598349.
        dhcp-name-match=set:wpad-ignore,wpad
        dhcp-ignore-names=tag:wpad-ignore

        # Define upstream DNS servers manually.
        no-resolv

        # Store leases in per-instance files.
        dhcp-leasefile=/var/lib/misc/dnsmasq.leases.{{ instance }}

        # We're the authoritative DHCP server here.
        dhcp-authoritative

        # Announce us to be the NTP server.
        dhcp-option=option:ntp-server,0.0.0.0

        # Provide DHCPv4 (and our own hostname).
        # Also make us the MQTT server.
        # TODO: `glotzilla` was the name before, remove it when ready.
      {% for subnet in instance_subnets %}
        # Subnet {{ subnet }}
        dhcp-range=10.115.{{ subnet }}.130,10.115.{{ subnet }}.199,6h
        dynamic-host={{ grains["id"] }},0.0.0.254,{{ vlan_if(subnet) }}
        dynamic-host=mqtt,0.0.0.254,{{ vlan_if(subnet) }}
        dynamic-host=glotzilla,0.0.0.254,{{ vlan_if(subnet) }}
      {% endfor %}

        # Have an easy alias for my Behringer XR18.
        cname=mischa,XR18-78-20-0C

    {% if instance == "unrestricted" %}
        # Forward most requests to the uplink.
        server=192.168.0.1

        # Read the host names provided by the restricted instance.
        addn-hosts={{ restricted_hosts_file }}

        # Provide our own hostname on lo.
        # We use Debian's convention of designating 127.0.1.1 to the name.
        dynamic-host={{ grains["id"] }},0.0.1.1,lo
        dynamic-host={{ grains["id"] }}.{{ domain }},0.0.1.1,lo

        # Keep providing the uplink router's DNS name.
        host-record=kabelbox,kabelbox.local,192.168.0.1
    {% else %}
        # Don't listen on lo, the unrestricted access does that.
        except-interface=lo

        # Create a hosts-style file for the DHCP names we manage.
        # This allows the "unrestricted" dnsmasq to know about them.
        dhcp-script={{ restricted_hosts_script }}

        # Hijack pool.ntp.org to point to us, so that the restricted devices
        # can at least get NTP without having to manually configure a server.
    {% for subnet in instance_subnets %}
        dynamic-host=pool.ntp.org,0.0.0.254,{{ vlan_if(subnet) }}
      {% for i in [0, 1, 2, 3] %}
        dynamic-host={{ i }}.pool.ntp.org,0.0.0.254,{{ vlan_if(subnet) }}
      {% endfor %}
    {% endfor %}
    {% endif %}
  service.running:
    - name: dnsmasq@{{ instance }}
    - enable: true
  cmd.run:
    - name: systemctl restart dnsmasq@{{ instance }}
    - onchanges:
        - file: /etc/default/dnsmasq.{{ instance }}
        - file: /etc/dnsmasq.{{ instance }}

{% endfor %}

# Enable IPv4 forwarding.
/etc/sysctl.d/landsitz-router.conf:
  file.managed:
    - user: root
    - group: root
    - mode: 0644
    - contents: |
        net.ipv4.ip_forward=1
  cmd.run:
    - name: sysctl -p /etc/sysctl.d/landsitz-router.conf
    - onchanges:
        - file: /etc/sysctl.d/landsitz-router.conf

# Firewall config.
/etc/nftables.conf:
  file.managed:
    - require:
        - pkg: router
        - file: {{ interfaces_file }}
    - user: root
    - group: root
    - mode: 0755
    - contents: |
        #!/usr/sbin/nft -f

        flush ruleset

        define allow_wan = { allow_all, allow_wan }
        define lan_ifgroup = { allow_all, allow_wan, isolated, isolated_noingress }
        define overlay_ifgroup = { scynet }

        # Input firewall.
        table inet firewall {
          chain input {
            type filter hook input priority filter; policy drop;
            # Allow established connections.
            ct state established,related accept
            # Loopback.
            iif lo accept
            # Ping requests.
            icmp type echo-request accept
            # DNS, DHCP & NTP.
            udp dport { 53, 67, 123 } iifgroup $lan_ifgroup accept
            # DNS & MQTT.
            tcp dport { 53, 1883 } iifgroup $lan_ifgroup accept
            # SSH & Motion (only via overlay networks).
            tcp dport { 22, 1984, 1985 } iifgroup $overlay_ifgroup accept
          }
        }

        # We are not filtering IPv6 here because we're not using it (yet).
        table ip filter {
          chain forward {
            type filter hook forward priority filter; policy drop;
            # Only route packets upstream if they're from an allowed interface.
            # Packets arriving via LAN cannot go out to another LAN interface.
            ct state established,related accept
            iifgroup $allow_wan oifgroup { uplink } accept
            # Never allow forwarding to isolated_noingress LANs, not even from
            # allow_all/allow_lan hosts.
            oifgroup { isolated_noingress } drop
            # Hosts in allow_all may establish connections to other VLANs.
            iifgroup { allow_all } oifgroup $lan_ifgroup accept
          }
        }

        # NAT masquerade every IPv4 packet going to the uplink.
        table ip nat {
          chain postrouting {
            type nat hook postrouting priority srcnat; policy accept;
            oifgroup { uplink } masquerade
          }
        }
  service.running:
    - name: nftables
    - enable: true
  cmd.run:
    - name: systemctl restart nftables
    - onchanges:
        - file: /etc/nftables.conf
        # Also re-setup nftables if the interfaces changed.
        - cmd: {{ interfaces_file }}
