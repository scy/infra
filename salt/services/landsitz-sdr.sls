{% set unit_name = "landsitz-sdr" %}
{% set unit_file = "/etc/systemd/system/" + unit_name + ".service" %}
{% set secrets_file = "/etc/secrets/landsitz-sdr.env" %}
{% set repo_base = "/opt/scy" %}
{% set repo_dir = repo_base + "/landsitz-sdr" %}
{% set runas_user = "sdr" %}
{% set user_home = "/home/" + runas_user %}
{% set rtl433_dir = user_home + "/rtl_433" %}
{% set rtl433_binary = "/usr/local/bin/rtl_433" %}

rtl_433 dependencies:
  pkg.installed:
    - pkgs:
        - build-essential
        - cmake
        - librtlsdr-dev
        - libtool
        - libusb-1.0-0-dev
        - pkg-config
        - rtl-sdr

SDR packages:
  pkg.installed:
    - pkgs:
        - python3-pydantic
        - python3-requests

SDR repo:
  git.cloned:
    - require:
        - file: {{ repo_base }}
    - name: https://codeberg.org/scy/landsitz-sdr.git
    - target: {{ repo_dir }}
    - user: scy

{{ runas_user }}:
  user.present:
    - home: {{ user_home }}
    - groups:
        - plugdev
    - remove_groups: false

rtl_433 repo:
  git.cloned:
    - require:
        - user: {{ runas_user }}
    - name: https://github.com/merbanan/rtl_433.git
    - target: {{ rtl433_dir }}
    - user: {{ runas_user }}
    - branch: "22.11"

build rtl_433:
  cmd.run:
    - unless:
        - test -e '{{ rtl433_binary }}'
    - require:
        - git: rtl_433 repo
    - name: 'mkdir -p build && cd build && cmake .. && make -j4'
    - cwd: {{ rtl433_dir }}
    - runas: {{ runas_user }}

install rtl_433:
  cmd.run:
    - unless:
        - test -e '{{ rtl433_binary }}'
    - require:
        - cmd: build rtl_433
    - name: install build/src/rtl_433 /usr/local/bin
    - cwd: {{ rtl433_dir }}

{{ secrets_file }}:
  file.managed:
    - user: root
    - group: root
    - mode: 0600
    - makedirs: true
    - dir_mode: 0700
    - contents:
      {% for secret in ["GRAPHITE_APIKEY", "GRAPHITE_URL"] %}
        - {{ secret }}={{ salt["pillar.get"]("secrets:" + secret) }}
      {% endfor %}

{{ unit_file }}:
  file.managed:
    - require:
        # TODO: Actually we should wait for rtl_433 to be installed.
        - git: SDR repo
        - file: {{ secrets_file }}
        - pkg: SDR packages
        - user: {{ runas_user }}
    - user: root
    - group: root
    - mode: 0755
    - contents: |
        [Unit]
        Description=Landsitz SDR listener
        Documentation=https://codeberg.org/scy/landsitz-sdr
        After=network.service

        [Service]
        User={{ runas_user }}
        Restart=always  # rtl_433 is called in a pipe without pipefail and thus exits with 0
        RestartSec=5
        EnvironmentFile={{ secrets_file }}
        Environment=PYTHONUNBUFFERED=y
        WorkingDirectory={{ repo_dir }}
        ExecStart={{ repo_dir }}/launch.sh

        [Install]
        WantedBy=default.target
  cmd.run:
    - onchanges:
        - file: {{ unit_file }}
    - name: systemctl daemon-reload

{{ unit_name }}:
  service.running:
    - require:
        - file: {{ unit_file }}
    - enable: true
