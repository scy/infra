# Disable salt-minion, we're running masterless.
salt-minion:
  service.dead:
    - enable: false
