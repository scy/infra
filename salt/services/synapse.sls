{% set server_name = salt["pillar.get"]("matrix:server_name") %}
{% set http_domain = salt["pillar.get"]("matrix:http_domain") %}
{% set redir_addresses = salt["pillar.get"]("matrix:redir_addresses") %}
{% set redir_target = salt["pillar.get"]("matrix:redir_target") %}
{% set base_url = "https://" ~ http_domain ~ "/" %}
{% set volume_id = salt["pillar.get"]("matrix:hetzner_volume") %}
{% set volume_path = "/dev/disk/by-id/scsi-0HC_Volume_" ~ volume_id %}
{% set volume_mount = "/mnt/volume" %}
{% set synapse_uid = 991 %}
{% set synapse_home = "/srv/synapse" %}
{% set data_dir = synapse_home ~ "/data" %}
{% set postgres_pass = salt["random.get_str"](64) %}

Synapse Hetzner volume:
  mount.fstab_present:
    - name: {{ volume_path }}
    - fs_file: {{ volume_mount }}
    - fs_vfstype: ext4
    - fs_mntops: discard,nofail,defaults

  file.directory:
    - name: {{ volume_mount }}
    - mode: 0755

  cmd.run:
    - require:
        - file: Synapse Hetzner volume
    - onchanges:
        - mount: Synapse Hetzner volume
    - name: "{ umount {{ volume_path }} || true; } && mount {{ volume_mount }}"

{% for dir in [synapse_home, "/var/lib/postgresql"] %}
Synapse mount point {{ dir }}:
  file.directory:
    - name: {{ dir }}
    - mode: 0755
{% endfor %}

Synapse data bind mount:
  mount.fstab_present:
    - require:
        - Synapse Hetzner volume
    - name: {{ volume_mount }}/synapse
    - fs_file: {{ synapse_home }}
    - fs_vfstype: none
    - fs_mntops: defaults,bind
    - mount: false

  file.directory:
    - name: {{ volume_mount }}/synapse
    - user: {{ synapse_uid }}
    - group: {{ synapse_uid }}
    - mode: 0750

  cmd.run:
    - require:
        - mount: Synapse data bind mount
        - Synapse mount point {{ synapse_home }}
    - onchanges:
        - mount: Synapse data bind mount
    - name: mount {{ synapse_home }}

# Create the user before actually installing the package so that we can prepare /var/lib/postgresql.
Postgres user:
  pkg.installed:
    - pkgs:
        - postgresql-common

Synapse Postgres bind mount:
  mount.fstab_present:
    - require:
        - Synapse Hetzner volume
    - name: {{ volume_mount }}/postgres
    - fs_file: /var/lib/postgresql
    - fs_vfstype: none
    - fs_mntops: defaults,bind
    - mount: false

  cmd.run:
    - require:
        - mount: Synapse Postgres bind mount
        - Synapse mount point /var/lib/postgresql
        - Postgres user
    - onchanges:
        - mount: Synapse Postgres bind mount
    - require_in:
        - Postgres package
    - name: "mkdir -p {{ volume_mount }}/postgres && chown postgres: {{ volume_mount }}/postgres && mount /var/lib/postgresql"

Synapse user:
  group.present:
    - name: synapse
    - gid: {{ synapse_uid }}

  user.present:
    - require:
        - group: Synapse user
        - Synapse data bind mount
    - name: synapse
    - uid: {{ synapse_uid }}
    - gid: {{ synapse_uid }}
    - fullname: Synapse Matrix Server
    - home: {{ synapse_home }}
    - create_home: true
    - shell: /bin/bash
    - remove_groups: false

  cmd.run:
    - unless:
        - test -e /var/lib/systemd/linger/synapse
    - name: loginctl enable-linger synapse

Synapse data dir:
  cmd.run:
    - require:
        - Synapse data bind mount
    - unless:
        - test -d {{ data_dir }}
    - name: "mkdir -p {{ data_dir }} && chmod 0750 {{ data_dir }} && chown synapse: {{ data_dir }}"

packages for Synapse:
  pkg.installed:
    - pkgs:
        - dbus-user-session
        - podman
        - slirp4netns
        - uidmap

/usr/local/bin/in-synapse-img:
  file.managed:
    - require:
        - packages for Synapse
        - Synapse data dir
    - mode: 0755
    - contents:
        #!/bin/sh
        # Run a command inside of the Synapse image.
        # Created by synapse.sls.

        exec sudo -u synapse podman run --rm -it --user {{ synapse_uid }}:{{ synapse_uid }} --userns keep-id --mount type=bind,src=/srv/synapse/data,dst=/data --mount type=bind,src=/var/run/postgresql/.s.PGSQL.5432,dst=/var/run/postgresql/.s.PGSQL.5432 --entrypoint '[]' docker://matrixdotorg/synapse:latest "$@"

Synapse Postgres user:
  postgres_user.present:
    - require:
        - Postgres package
    - name: synapse
    - password: "{{ postgres_pass }}"
    - encrypted: true

Synapse Postgres database:
  postgres_database.present:
    - require:
        - Synapse Postgres user
    - name: synapse
    - owner: synapse
    - template: template0
    - encoding: UTF8
    - lc_collate: C
    - lc_ctype: C

Synapse secrets:
  file.managed:
    - require:
        - Synapse Postgres database
    - name: {{ data_dir }}/secrets.yaml
    - user: synapse
    - group: synapse
    - mode: 0600
    - replace: false
    - contents: |
        # Created by synapse.sls.

        form_secret: "{{ salt["random.get_str"](64) }}"
        macaroon_secret_key: "{{ salt["random.get_str"](64) }}"
        registration_shared_secret: "{{ salt["random.get_str"](64) }}"

        database:
          name: psycopg2
          args:
            user: synapse
            password: "{{ postgres_pass }}"
            database: synapse
            cp_min: 5
            cp_max: 10
          # name: sqlite3
          # args:
          #   database: /data/homeserver.db

Synapse log config:
  file.managed:
    - require:
        - /usr/local/bin/in-synapse-img
    - name: {{ data_dir }}/log.config
    - user: synapse
    - group: synapse
    - mode: 0644
    - contents: |
        # Created by synapse.sls.

        version: 1
        formatters:
            precise:
                format: '%(asctime)s - %(name)s - %(lineno)d - %(levelname)s - %(request)s - %(message)s'
        handlers:
            console:
                class: logging.StreamHandler
                formatter: precise
        loggers:
            synapse.storage.SQL:
                # beware: increasing this to DEBUG will make synapse log sensitive
                # information such as access tokens.
                level: INFO
        root:
            level: INFO
            handlers: [console]
        disable_existing_loggers: false

Synapse signing key:
  cmd.run:
    - require:
        - /usr/local/bin/in-synapse-img
    - unless:
        - test -e {{ data_dir }}/signing.key
    - cwd: /
    - name: in-synapse-img generate_signing_key -o /data/signing.key

Synapse config:
  file.managed:
    - require:
        - Synapse log config
        - Synapse signing key
    - name: {{ data_dir }}/homeserver.yaml
    - user: synapse
    - group: synapse
    - mode: 0660
    - contents: |
        # Managed by synapse.sls.

        server_name: {{ server_name }}
        public_baseurl: {{ base_url }}
        pid_file: /data/homeserver.pid
        listeners:
          - port: 8008
            tls: false
            type: http
            x_forwarded: true
            resources:
              - names: [client, federation]
                compress: false
        log_config: "/data/log.config"
        media_store_path: /data/media_store
        signing_key_path: /data/signing.key
        report_stats: true
        trusted_key_servers:
          - server_name: "matrix.org"

/usr/local/bin/synapse-in-podman:
  file.managed:
    - require:
        - Synapse config
    - mode: 0755
    - contents: |
        #!/bin/sh
        # Start Synapse in a Podman container.
        # Created by synapse.sls.

        export PYTHONUNBUFFERED=y
        cd /
        exec sudo -u synapse podman run --rm --user {{ synapse_uid }}:{{ synapse_uid }} --userns keep-id -v /dev/log:/dev/log --mount type=bind,src={{ data_dir }},dst=/data --mount type=bind,src=/var/run/postgresql/.s.PGSQL.5432,dst=/var/run/postgresql/.s.PGSQL.5432 -p 8008:8008 docker://matrixdotorg/synapse:latest run -c /data/homeserver.yaml -c /data/secrets.yaml

Synapse systemd unit:
  file.managed:
    - require:
        - /usr/local/bin/synapse-in-podman
    - name: /etc/systemd/system/synapse.service
    - mode: 0644
    - contents: |
        [Unit]
        Description=Synapse Matrix server
        After=network.service

        [Service]
        Restart=always
        RestartSec=5
        ExecStart=/usr/local/bin/synapse-in-podman

        [Install]
        WantedBy=default.target

  cmd.run:
    - onchanges:
        - file: Synapse systemd unit
    - name: systemctl daemon-reload

  service.running:
    - name: synapse.service
    - require:
        - cmd: Synapse systemd unit
    - enable: true

Synapse reverse proxy config:
  file.managed:
    - require:
        - Caddy package
    - name: /etc/caddy/Caddyfile
    - mode: 0644
    - contents: |
        {{ http_domain }} {
          reverse_proxy /_matrix/* localhost:8008
          reverse_proxy /_synapse/client/* localhost:8008
        }

        {{ http_domain }}:8448 {
          reverse_proxy localhost:8008
        }

        {{ redir_addresses }} {
          redir {{ redir_target }} temporary
        }

  cmd.run:
    - onchanges:
        - file: Synapse reverse proxy config
    - name: systemctl reload caddy
