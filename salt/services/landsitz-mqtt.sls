{% set unit_name = "landsitz-mqtt" %}
{% set unit_file = "/etc/systemd/system/" + unit_name + ".service" %}
{% set secrets_file = "/etc/secrets/" + unit_name + ".env" %}
{% set repo_base = "/opt/scy" %}
{% set repo_dir = repo_base + "/" + unit_name %}
{% set runas_user = "mqtt" %}

Landsitz MQTT packages:
  pkg.installed:
    - pkgs:
        - python3-paho-mqtt
        - python3-requests

Landsitz MQTT repo:
  git.cloned:
    - require:
        - file: {{ repo_base }}
    - name: https://codeberg.org/scy/landsitz-mqtt.git
    - target: {{ repo_dir }}
    - user: scy

{{ runas_user }}:
  user.present

{{ secrets_file }}:
  file.managed:
    - user: root
    - group: root
    - mode: 0600
    - makedirs: true
    - dir_mode: 0700
    - contents:
      {% for secret in ["GRAPHITE_APIKEY", "GRAPHITE_URL", "PUSHOVER_USER", "PUSHOVER_ALERT_APP", "PUSHOVER_BELL_APP", "PUSHOVER_FLAP_APP"] %}
        - {{ secret }}={{ salt["pillar.get"]("secrets:" + secret) }}
      {% endfor %}

{{ unit_file }}:
  file.managed:
    - require:
        - git: Landsitz MQTT repo
        - file: {{ secrets_file }}
        - pkg: Landsitz MQTT packages
        - user: {{ runas_user }}
    - user: root
    - group: root
    - mode: 0755
    - contents: |
        [Unit]
        Description=Landsitz MQTT listener
        Documentation=https://codeberg.org/scy/landsitz-mqtt
        After=network.service

        [Service]
        User={{ runas_user }}
        Restart=on-failure
        RestartSec=5
        EnvironmentFile={{ secrets_file }}
        WorkingDirectory={{ repo_dir }}
        ExecStart=/usr/bin/python3 {{ repo_dir }}/watch_mqtt.py

        [Install]
        WantedBy=default.target
  cmd.run:
    - onchanges:
        - file: {{ unit_file }}
    - name: systemctl daemon-reload

{{ unit_name }}:
  service.running:
    - require:
        - file: {{ unit_file }}
    - enable: true
