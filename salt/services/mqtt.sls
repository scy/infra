MQTT packages:
  pkg.installed:
    - pkgs:
        - mosquitto
        - mosquitto-clients

/etc/mosquitto/conf.d/scy.conf:
  file.managed:
    - require:
        - pkg: MQTT packages
    - user: root
    - group: root
    - mode: 0644
    - contents: |
        allow_anonymous true
        listener 1883
  cmd.run:
    - name: systemctl restart mosquitto
    - onchanges:
        - file: /etc/mosquitto/conf.d/scy.conf
