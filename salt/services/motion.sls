{% set fps = 4 %}
{% set loopback_map_file = "/etc/motion/loopback-mapping" %}
{% set loopback_offset = 10 %}
{% set cameras = salt["pillar.get"]("motion:cameras") %}
{% set camera_numbers | trim %}
  {% set comma = joiner(",") %}
  {% for cam in cameras.values() %}{{ comma() }}{{ cam.id + loopback_offset }}{% endfor %}
{% endset %}

motion:
  pkg.installed:
    - pkgs:
        - ffmpeg
        - imagemagick
        - motion

allow hw accel for motion:
  user.present:
    - name: motion
    - require:
        - pkg: motion
    # Don't create it.
    - onlyif:
        - id motion >/dev/null 2>&1
    - groups:
        - render
    - remove_groups: false

/srv/motion:
  file.directory:
    - user: motion
    - group: root
    - mode: 0750

/etc/motion/motion.conf:
  file.managed:
    - require:
        - pkg: motion
        - file: /srv/motion
    - user: root
    - group: motion
    - mode: 0640
    - contents: |
        # Have the web interface on port 1984.
        webcontrol_port 1984
        # TODO: Maybe reverse proxy this.
        webcontrol_localhost off

        # Stream images on port 1985.
        stream_port 1985
        # TODO: Maybe reverse proxy this.
        stream_localhost off

        # Don't do motion detection (for now).
        threshold 2000000000

        # Capture settings.
        pre_capture 5
        post_capture 300
        event_gap 30

        target_dir /srv/motion

        # Create a timelapse.
        timelapse_interval 24
        timelapse_fps 30
        timelapse_mode daily
        timelapse_filename %Y-%m-%d Timelapse %$

        # Design.
        text_changes on
        text_event %Y-%m-%d %H-%M-%S E%v D%DL%Q %$
        text_left %C
        text_right %$\n%Y-%m-%d\n%T

        # Quality settings.
        framerate {{ fps }}
        despeckle_filter EedDl
        picture_quality 95
        stream_quality 75

        camera_dir /etc/motion/cameras

        # We don't set `log_file` so that the logs go to stdout/stderr.
  cmd.run:
    - name: systemctl restart motion
    - onchanges:
        - file: /etc/motion/motion.conf

{% for name, cam in cameras.items() %}
/etc/motion/cameras/{{ name }}.conf:
  file.managed:
    - require:
        - pkg: motion
    - require_in:
        - file: /etc/motion/motion.conf
    - onchanges_in:
        - cmd: /etc/motion/motion.conf
    - user: motion
    - group: root
    - mode: 0640
    - makedirs: true
    - dir_mode: 0750
    - contents: |
        camera_id {{ cam.id }}
        camera_name {{ name }}

        width {{ cam.get("width", 640) }}
        height {{ cam.get("height", 480) }}

        videodevice /dev/video{{ cam.id + loopback_offset }}
{% endfor %}

create {{ loopback_map_file }}:
  file.managed:
    - name: {{ loopback_map_file }}
    - user: motion
    - group: root
    - mode: 0640
    - replace: false
    - require:
        - pkg: motion

populate {{ loopback_map_file }}:
  file.keyvalue:
    - require:
        - file: create {{ loopback_map_file }}
    - name: {{ loopback_map_file }}
    - separator: " "
    - append_if_not_found: true
    - key_values:
      {% for cam in cameras.values() %}
        "{{ cam.id + loopback_offset }}": {{ cam.source }}
      {% endfor %}

/usr/local/bin/ffmpeg-motion:
  file.managed:
    - user: root
    - group: root
    - mode: 0755
    - contents: |
        #!/bin/sh
        set -e

        dev="$1"; shift
        input="$(awk "\$1 == $dev { print \$2 }" {{ loopback_map_file }})"

        LIBVA_DRIVER_NAME=i965

        v4l2loopback-ctl set-fps {{ fps }} "/dev/video$dev"
        v4l2-ctl -d "$dev" -c timeout=5000

        # FIXME: This can leak passwords in the URL to local users.
        # <https://superuser.com/a/1393990/45922> might be a solution.
        exec sudo -u motion ffmpeg \
          -hwaccel vaapi -hwaccel_device /dev/dri/renderD128 \
          -max_delay 5000000 \
          -i "$input" \
          -stimeout 5000000 \
          -reconnect 1 -reconnect_streamed 1 \
          -filter:v fps={{ fps }} \
          -f v4l2 -pix_fmt yuv420p \
          "/dev/video$dev"

/usr/local/bin/check-ffmpeg-motion:
  file.managed:
    - user: root
    - group: root
    - mode: 0755
    - contents: |
        #!/bin/sh
        set -e

        # Take a frame from the Motion feed and check whether it contains >= 100k pixels
        # in ffmpeg's "failure" color. If so, restart the feed.

        # Requires ffmpeg and ImageMagick.

        : ${LOOPBACK_OFFSET:=10} ${MOTION_URL:=http://localhost:1985}

        for stream_num in "$@"; do
        	loopback_num="$((stream_num + LOOPBACK_OFFSET))"
        	if ffmpeg \
        		-loglevel error \
        		-i "$MOTION_URL/$stream_num/stream" \
        		-frames:v 1 \
        		-c:v png -f image2pipe - \
        	| convert - \
        		-format %c histogram:info:- \
        	| grep -qs '^[[:space:]]*[0-9]\{6,\}:.* #008700 '; then
        		echo "Restarting broken camera #$stream_num (/dev/video$loopback_num)..."
        		if systemctl restart "ffmpeg-motion@$loopback_num"; then
        			echo ok
        		else
        			echo "failed ($?)"
        		fi
        	fi
        done

/etc/systemd/system/v4l2loopback-motion.service:
  file.managed:
    - require:
        - pkg: v4l2loopback
    - user: root
    - group: root
    - mode: 0644
    - contents: |
        [Unit]
        Description=v4l2loopback devices for Motion
        Before=motion.service

        [Service]
        Type=oneshot
        RemainAfterExit=yes
        ExecStart=/usr/sbin/modprobe v4l2loopback video_nr={{ camera_numbers }}
        ExecStop=/usr/sbin/modprobe -r v4l2loopback

        [Install]
        WantedBy=motion.service
  cmd.run:
    - onchanges:
        - file: /etc/systemd/system/v4l2loopback-motion.service
    - name: systemctl daemon-reload
  service.running:
    - name: v4l2loopback-motion
    - enable: true

/etc/systemd/system/ffmpeg-motion@.service:
  file.managed:
    - require:
        - file: /usr/local/bin/ffmpeg-motion
        - pkg: v4l2loopback
    - user: root
    - group: root
    - mode: 0644
    - contents: |
        [Unit]
        Description=ffmpeg camera stream for Motion
        Before=motion.service
        Requires=v4l2loopback-motion.service

        [Service]
        Type=exec
        ExecStart=/usr/local/bin/ffmpeg-motion %i
        Restart=on-failure
        RestartSec=10
        TimeoutStopSec=5

        [Install]
        WantedBy=motion.service
  cmd.run:
    - onchanges:
        - file: /etc/systemd/system/ffmpeg-motion@.service
    - name: systemctl daemon-reload

/etc/systemd/system/check-ffmpeg-motion.service:
  file.managed:
    - require:
        - file: /usr/local/bin/check-ffmpeg-motion
        - pkg: motion
    - user: root
    - group: root
    - mode: 0644
    - contents: |
        # This service is activated by its corresponding timer.

        [Unit]
        Description=restart broken Motion ffmpeg camera streams
        After=motion.service
        Requires=motion.service

        [Service]
        Type=oneshot
        ExecStart=/usr/local/bin/check-ffmpeg-motion{% for cam in cameras.values() %} {{ cam.id }}{% endfor %}

/etc/systemd/system/check-ffmpeg-motion.timer:
  file.managed:
    - require:
        - file: /etc/systemd/system/check-ffmpeg-motion.service
    - user: root
    - group: root
    - mode: 0644
    - contents: |
        [Unit]
        Description=regular checks and restarts of Motion camera feeds

        [Timer]
        # Start 2 minutes after boot.
        OnBootSec=2min
        # And then 30 seconds after the last check.
        OnUnitInactiveSec=30

        [Install]
        WantedBy=timers.target

{% for cam in cameras.values() %}
ffmpeg-motion@{{ cam.id + loopback_offset }}:
  service.running:
    - require:
        - file: /etc/systemd/system/ffmpeg-motion@.service
    - enable: true
  cmd.run:
    - onchanges:
        - file: /usr/local/bin/ffmpeg-motion
    - name: systemctl restart ffmpeg-motion@{{ cam.id + loopback_offset }}
{% endfor %}

check-ffmpeg-motion units:
  cmd.run:
    - onchanges:
        - file: /etc/systemd/system/check-ffmpeg-motion.service
        - file: /etc/systemd/system/check-ffmpeg-motion.timer
    - name: systemctl daemon-reload
  service.running:
    - require:
        - file: /etc/systemd/system/check-ffmpeg-motion.timer
    - name: check-ffmpeg-motion.timer
    - enable: true
