SSH packages:
  pkg.installed:
    - pkgs:
        - openssh-server

SSH config:
  file.keyvalue:
    - require:
        - pkg: SSH packages
    - name: /etc/ssh/sshd_config
    - separator: " "
    - uncomment: "# "
    - append_if_not_found: true
    - key_ignore_case: true
    - key_values:
        PasswordAuthentication: "no"
        PermitRootLogin: without-password  # Old, misleading name for `prohibit-password`.
  cmd.run:
    - name: systemctl restart ssh
    - onchanges:
        - file: /etc/ssh/sshd_config
