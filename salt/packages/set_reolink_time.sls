{% set me = "set_reolink_time" %}
{% set repo_base = "/opt/scy" %}
{% set repo_dir = repo_base + "/" + me %}
{% set secrets_file = "/etc/secrets/" + me + ".env" %}
{% set runas_user = "reolinktime" %}

{{ me }}:
  pkg.installed:
    - pkgs:
        - git
        - python3-requests
  git.cloned:
    - require:
        - file: {{ repo_base }}
        - pkg: {{ me }}
    - name: https://codeberg.org/scy/{{ me }}.git
    - target: {{ repo_dir }}
    - user: scy

{{ me }} user:
  user.present:
    - name: {{ runas_user }}

{{ secrets_file }}:
  file.managed:
    - user: {{ runas_user }}
    - group: root
    - mode: 0600
    - makedirs: true
    - dir_mode: 0700
    - contents:
        - REOLINK_PASSWORD='{{ salt["pillar.get"]("secrets:REOLINK_PASSWORD") }}'
