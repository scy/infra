v4l2loopback:
  pkg.installed:
    - pkgs:
        - v4l2loopback-dkms
        - v4l2loopback-utils
