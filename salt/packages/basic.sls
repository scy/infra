basic packages:
  pkg.installed:
    - pkgs:
{% if grains["os"] == "Debian" %}
        - bind9-host  # my favorite DNS tool
        - dnsutils  # for dig etc.
        - libterm-readkey-perl  # for `git add -p` et al
        - netcat-openbsd
        - rdate
        - vim
{% elif grains["os"] == "Fedora" %}
        - bind-utils  # also includes dig
        - perl-TermReadKey
        - netcat
        - openrdate
        - vim-enhanced
{% endif %}
        - curl
        - file
        - git
        - hdparm
        - iotop
        - man-db
        - nethogs
        - pv
        - rhash
        - rsync
        - smartmontools
        - tmux
        - visidata
        - whois
        - wireguard-tools
