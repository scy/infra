{% set rclone_version = "1.58.0" %}
{% set rclone_deb = "/tmp/rclone-" ~ rclone_version ~ ".deb" %}
{% set rclone_deb_hash = "6d9bcf33e669fa907e4b1411ce3775a23e8660e666366e546f685fa8ebdc326af03978f680dfbd6035773b7dfdead20a7b8a29e445d3b1db0159a44fe0db0003" %}

install rclone:
  # This download will be cached automatically, no need to get fancy.
  file.managed:
    - name: {{ rclone_deb }}
    - source: https://downloads.rclone.org/v{{ rclone_version }}/rclone-v{{ rclone_version }}-linux-{{ grains["osarch"] }}.deb
    - source_hash: "{{ rclone_deb_hash }}"
  pkg.installed:
    - sources:
        - rclone: {{ rclone_deb }}
