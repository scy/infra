{% set sourceslist = "/etc/apt/sources.list" %}

Debian non-free:
  # Not using pkgrepo.managed here because it would require me to select a
  # static mirror, and also I'm kind of iffy on whether it supports combining
  # multiple "components" (like "main" and "non-free"), e.g. from other states.
  file.replace:
    - name: {{ sourceslist }}
    - pattern: '^(\s*deb\s+\S+\s+{{ grains["oscodename"] }}\s+\S.*)$'
    - repl: '\1 non-free'
    - bufsize: file  # https://github.com/saltstack/salt/issues/57584
    - unless:
        - 'grep -q " {{ grains["oscodename"] }} .* non-free.*" {{ sourceslist }}'
  cmd.run:
    - name: apt update
    - onchanges:
        - file: {{ sourceslist }}
