scy:

  pkg.installed:
    - pkgs:
        - git
        - rsync

  user.present:
    - require:
        - pkg: scy
    - fullname: Tim Weber
    - home: /home/scy
    - shell: /bin/bash
    - optional_groups:
        - adm
        - audio
        - cdrom
        - dialout
        - games
        - gpio
        - i2c
        - input
        - netdev
        - plugdev
        - spi
        - sudo
        - users
        - video
    - remove_groups: false

  git.cloned:
    - require:
        - user: scy
    # ~/dotfiles, after being cloned, will be moved to _become_ the home
    # directory. In order to not run into an endless loop where it will be
    # checked out again on each state apply, check whether the home directory
    # is already a Git repo.
    - unless:
        - test -d /home/scy/.git
    - name: https://codeberg.org/scy/dotfiles.git
    - target: /home/scy/dotfiles
    - user: scy

  cmd.run:
    - onchanges:
        - git: scy
    - runas: scy
    - cwd: /home/scy
    - umask: "022"
    - name: "test ! -e .orig_home && cd dotfiles && git submodule update --init && cd .. && rsync -avb --backup-dir=.orig_home dotfiles/ . && rm -rf dotfiles"
