{% for target in ["sleep", "suspend", "hibernate", "hybrid-sleep"] %}
never {{ target }} the system:
  service.masked:
    - name: {{ target }}.target
{% endfor %}
