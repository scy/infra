base:
  "*":
    - packages.basic
    - services.no-minion
  # Servers.
  fractal.scy.name or nucl or matrix-*:
    - power.no-sleep
    - users.scy
  fractal.scy.name:
    - bases.dconf-user
    - fonts.iosevka
    - networking.fractal
    - networking.systemd-networkd
    - screen.dark-mode
  matrix-*:
    - packages.caddy
    - packages.postgres
    - security.sudo-nopasswd
    - services.synapse
  nucl:
    - backup.borg
    - bases.opt-scy
    - packages.workstation
    - packages.non-free
    - packages.i965-va-driver-shaders
    - packages.rclone
    - packages.set_reolink_time
    - packages.v4l2loopback
    - screen.rotate
    - security.cryptsetup-boot-keyfile
    - security.auto-update
    - services.landsitz-mqtt
    - services.landsitz-router
    - services.landsitz-sdr
    - services.motion
    - services.mqtt
    - services.ntp
    - services.ssh
