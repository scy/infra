{% set rotate = pillar.get("screen", {}).get("rotate") %}

{% if rotate is defined %}
{% if rotate in [90, "90cw", "270ccw"] %}
  {% set fbcon_rotate = 1 %}
{% elif rotate in [180, "180cw", "180ccw"] %}
  {% set fbcon_rotate = 2 %}
{% elif rotate in [270, "270cw", "90ccw"] %}
  {% set fbcon_rotate = 3 %}
{% else %}
  {% set fbcon_rotate = 0 %}
{% endif %}

sysfsutils:
  pkg:
    - installed

/etc/sysfs.d/screen.rotate.conf:
  file.managed:
    - require:
        - pkg: sysfsutils
    - contents:
        - class/graphics/fbcon/rotate_all = {{ fbcon_rotate }}
  cmd.run:
    - name: systemctl restart sysfsutils
    - onchanges:
        - file: /etc/sysfs.d/screen.rotate.conf

{% endif %}
