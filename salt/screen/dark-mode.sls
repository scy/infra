set GNOME to dark mode:
  file.managed:
    - require:
        - file: dconf user profile
    - name: /etc/dconf/db/local.d/50_scy_dark
    - mode: 0644
    - makedirs: true
    - contents: |
        [org/gnome/desktop/interface]
        gtk-theme='Adwaita-dark'
  cmd.run:
    - onchanges:
        - file: set GNOME to dark mode
    - name: dconf update
