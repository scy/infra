# Salt Configuration

I'm running Salt in masterless mode on my machines.

## Bootstrapping Salt

1. You'll need this repository checked out somewhere.
   I usually have it at `/home/scy/proj/infra` (if there is a `scy` user account on the machine) or `/root/infra` (if there isn't).
2. Make sure `/srv/salt` points to this directory:
   `ln -s ../home/scy/infra/salt /srv/salt`
3. Install Salt, either by something like `apt install salt-common`, or by using [Salt Bootstrap](https://github.com/saltstack/salt-bootstrap).
4. Check `/etc/salt/minion_id`.
5. Put the following into `/etc/salt/minion.d/scy.conf`:

   ```yaml
   file_client: local
   pillar_raise_on_missing: true
   ```
6. Create `/srv/pillar/top.sls` with the following content:

   ```yaml
   base:
     "*":
       - local
   ```
7. Create `/srv/pillar/local.sls` and [make sure it's only readable by root](https://unix.stackexchange.com/a/18002/10382): `sudo sh -c 'umask 0077 && touch /srv/pillar/local.sls'`
8. Put in this file whatever is needed or required for the system.
   Since the pillar is used for local configuration and secrets, there are no examples in this repo, but you can find a list of the values I'm using below.
9. Run `sudo salt-call --local state.apply`.

## Pillar Values

These are the values that **can** exist in a `/srv/pillar/local.sls`, depending on the machine.

* `borg`: Configuration settings for [Borg](https://borgbackup.org/). Currently only one destination is supported.
  * `alias`: How to call the host in the SSH config that will be generated. Defaults to `borg`.
  * `host`: Host name to back up to.
  * `port`: Port number to use. Defaults to `22`.
  * `repo`: Path on the remote machine to use as the repository. Defaults to `borg`.
  * `user`: User name to log in as.
* `matrix`: Configuration settings for Matrix servers (currently: Synapse).
  * `hetzner_volume`: The Hetzner Cloud volume ID to store the server’s data on.
  * `http_domain`: The domain at which the Matrix API and media storage should be available. Strongly recommended to be a completely different domain than `server_name`.
  * `redir_addresses`: [Caddy addresses](https://caddyserver.com/docs/caddyfile/concepts#addresses) that should be redirected by the reverse proxy to `redir_target`. Mainly for providing some sort of content in case someone manually looks at the server in their browser.
  * `redir_target`: The target URL to redirect to.
  * `server_name`: The publicly visible domain name that will be used for accounts on this server.
* `screen`
  * `rotate`: Whether and how to rotate the attached screen. Can be set to `{90,180,270}{cw,ccw}`, e.g. `90cw`. Used by [`screen/rotate.sls`](screen/rotate.sls).

### Secrets

These live under the `secrets` key.

* `BORG_PASSPHRASE`: Passphrase for the Borg repo.
* `GRAPHITE_APIKEY`: API key to send data to the Graphite instance in the [Grafana Cloud](https://grafana.com/products/cloud/) account I'm using.
* `GRAPHITE_URL`: URL to the Graphite instance.
* `PUSHOVER_USER`: User key at Pushover.
* `PUSHOVER_BELL_APP`: App key for my Pushover door bell application.
* `PUSHOVER_FLAP_APP`: App key for my Pushover letter box application.
* `REOLINK_PASSWORD`: Admin password for the Reolink IP cameras, used to set their time.
