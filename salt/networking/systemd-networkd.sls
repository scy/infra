disable NetworkManager:
  service.dead:
    - name: NetworkManager
    - enable: false

use systemd-networkd:
  service.running:
    - require:
        - service: disable NetworkManager
    - name: systemd-networkd
    - enable: true
