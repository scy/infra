/etc/systemd/network/10-uplink.link:
  file.managed:
    - contents: |
        [Match]
        MACAddress=3c:ec:ef:93:fe:72

        [Link]
        Description=Main Uplink (Vodafone)
        Name=uplink

/etc/systemd/network/10-uplink.network:
  file.managed:
    - contents: |
        [Match]
        Name=uplink

        [Network]
        Address=192.168.0.250/24
        Gateway=192.168.0.1
        DNS=192.168.0.1

apply fractal network configuration:
  cmd.run:
    - onchanges:
        - file: /etc/systemd/network/10-uplink.network
    - name: systemctl reload systemd-networkd
