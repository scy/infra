dconf user profile:
  file.managed:
    - name: /etc/dconf/profile/user
    - mode: 0644
    - contents: |
        user-db:user
        system-db:local
