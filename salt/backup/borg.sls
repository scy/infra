{% set secrets_file = "/etc/secrets/borg.sh" %}
{% set sshkey_file = "/root/.ssh/id_ed25519" %}
{% set sshconfig_file = "/root/.ssh/config" %}

{% set borg_host = salt["pillar.get"]("borg:host") %}
{% set borg_alias = salt["pillar.get"]("borg:alias", "borg") %}
{% set borg_user = salt["pillar.get"]("borg:user") %}
{% set borg_port = salt["pillar.get"]("borg:port", 22) %}
{% set borg_repo = salt["pillar.get"]("borg:repo", "borg") %}
{% set borg_sources = salt["pillar.get"]("borg:sources", None) %}
{% set borg_pingurl = salt["pillar.get"]("borg:ping-url", "") %}
{% set borg_onefs = salt["pillar.get"]("borg:one-file-system", True) %}

borg packages:
  pkg.installed:
    - pkgs:
        - borgbackup

{{ secrets_file }}:
  file.managed:
    - user: root
    - group: root
    - mode: 0600
    - makedirs: true
    - dir_mode: 0700
    - contents:
        - ": ${BORG_REPO:='{{ borg_alias }}:{{ borg_repo }}'} ${BORG_PASSPHRASE:='{{ salt['pillar.get']('secrets:BORG_PASSPHRASE') }}'}"
        - export BORG_REPO BORG_PASSPHRASE

# Ensure root has an SSH key.
borg SSH key:
  cmd.run:
    - unless:
        - test -e '{{ sshkey_file }}'
    - name: ssh-keygen -t ed25519 -f '{{ sshkey_file }}' -N ''

# Make sure .ssh/config exists.
borg create {{ sshconfig_file }}:
  file.managed:
    - name: {{ sshconfig_file }}
    - makedirs: true
    - replace: false

# Create an SSH alias.
borg SSH alias:
  file.blockreplace:
    - require:
        - file: borg create {{ sshconfig_file }}
    - name: {{ sshconfig_file }}
    - append_if_not_found: true
    - marker_start: "### START Managed by backup/borg.sls ###"
    - marker_end: "### END Managed by backup/borg.sls ###"
    - content: |
        Host {{ borg_alias }}
          User {{ borg_user }}
          HostName {{ borg_host }}
          Port {{ borg_port }}

# Create a wrapper script.
/usr/local/sbin/sysborg:
  file.managed:
    - user: root
    - group: root
    - mode: 0755
    - contents: |
        #!/bin/sh

        . {{ secrets_file }}

      {% if borg_sources %}

        PING_URL='{{ borg_pingurl }}'

        autocreate() {
          archive="$1"; shift
          borg create \
            --stats --show-rc -C auto,lzma --list --filter=AME \
            {% if borg_onefs %}--one-file-system{% endif %} \
            "$@" "::$archive" \{% for source in borg_sources %}
            '{{ source }}' \
          {%- endfor %}
            2>&1
          rc=$?
          if [ "$rc" -eq 0 -a -n "$PING_URL" ]; then
            curl -s --max-time 10 "$PING_URL"
            echo
          fi
        }

        if [ "$1" = 'autocreate' ]; then
          shift
          now="$(date '+%Y-%m-%d.%H-%M')"
          archive="$(hostname -s).auto.$now"
          autocreate "$archive" "$@" | tee /root/.borg.log.running
          mv /root/.borg.log.running "/root/.borg.log.$now"
        else
          borg "$@"
        fi
      {% else %}
        borg "$@"
      {% endif %}

# Create a systemd unit & timer to do backups automatically.
/etc/systemd/system/sysborg.service:
  file.managed:
    - require:
        - file: /usr/local/sbin/sysborg
    - user: root
    - group: root
    - mode: 0644
    - contents: |
        # This service is activated by its corresponding timer.

        [Unit]
        Description=Full System Backup
        After=network.service

        [Service]
        Type=oneshot
        ExecStart=/usr/local/sbin/sysborg autocreate

/etc/systemd/system/sysborg.timer:
  file.managed:
    - require:
        - file: /etc/systemd/system/sysborg.service
    - user: root
    - group: root
    - mode: 0644
    - contents: |
        [Unit]
        Description=Regular Full System Backups

        [Timer]
        # Start 23 minutes after boot.
        OnBootSec=23min
        # And then 6 hours after the last backup completed.
        OnUnitInactiveSec=6h

        [Install]
        WantedBy=timers.target

sysborg units:
  cmd.run:
    - onchanges:
        - file: /etc/systemd/system/sysborg.service
        - file: /etc/systemd/system/sysborg.timer
    - name: systemctl daemon-reload

  service.running:
    - require:
        - file: /etc/systemd/system/sysborg.timer
    - name: sysborg.timer
    - enable: true
