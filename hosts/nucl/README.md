# nucl

* **Role:** Home Router, Home Automation Server, NAS
* **Named After:** Sounds like "Nuckel", a cute German word for someone or something that sucks on something. Pun on the fact that it's a NUC.

## Hardware

* Intel NUC5PPYH (Pentium N3700, 8 GB RAM)

It's using an ITE IT8607E Super-I/O chip at address `0xa40`.
The `it87` kernel driver does not support this chip, therefore there's no software fan control.
But the `coretemp` module at least provides CPU temperatures.

## Bootstrap

Install Debian 11 amd64.
(The disk setup is a bit unusual, but not documented here.)
Don't set a root password.
Instead, create a user `scy`.

Since nucl is supposed to be the router, the uplink Vodafone router it's connected to doesn't do DHCP.
Also, the uplink is using a TP-Link UE300 USB ethernet adapter, which is going to have a MAC-based network name and won't be brought up automatically by Debian.
Either take the network config manually from the Salt config in this repo, or create a makeshift one, e.g. by putting this into `/etc/network/interfaces.d/nucl`:

```
auto enx60a4b7c07538

iface enx60a4b7c07538 inet static
	address 192.168.0.254/24
	gateway 192.168.0.1
	dns-nameservers 192.168.0.1
```

Next, get the packages required to bootstrap Salt and place some initial config.

```sh
sudo apt update
sudo apt install git salt-minion
mkdir ~scy/proj
cd ~scy/proj
git clone https://codeberg.org/scy/infra.git
sudo ln -s ../home/scy/infra/salt /srv/salt
sudo cp ~scy/proj/infra/hosts/nucl/minion_id /etc/salt
```

Create `/srv/pillar/top.sls` and `/srv/pillar/local.sls` as described in [Bootstrapping Salt](../../salt/README.md).
`local.sls` will need something like this:

```yaml
screen:
  rotate: 90ccw
secrets:
  ...
```

Before applying the state, remove the network config so that it doesn't conflict with the Salt-generated one.
Then, apply the state.

```sh
sudo rm /etc/network/interfaces.d/nucl
sudo salt-call --local state.apply
```
